import { Component, OnInit } from '@angular/core';
import { InventariosService } from 'src/app/services/inventario/inventarios.service';
@Component({
  selector: 'app-mostrarproductos',
  templateUrl: './mostrarproductos.component.html',
  styleUrls: ['./mostrarproductos.component.css']
})
export class MostrarproductosComponent implements OnInit {

  public inventarios : any[] = [];
  constructor( private _inven: InventariosService) { }

  ngOnInit(): void {
    this.getTodosInventarios();
  }
  getTodosInventarios(){
    this._inven.getInventario()
    .subscribe((data: any) => {
      console.log(data);
      this.inventarios = data.data;
    })
  }
}
