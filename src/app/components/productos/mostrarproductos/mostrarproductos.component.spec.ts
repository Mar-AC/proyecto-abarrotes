import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarproductosComponent } from './mostrarproductos.component';

describe('MostrarproductosComponent', () => {
  let component: MostrarproductosComponent;
  let fixture: ComponentFixture<MostrarproductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarproductosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarproductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
