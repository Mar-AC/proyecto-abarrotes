import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Proveedores } from 'src/app/config/config';
import { ProveedoresService } from 'src/app/services/proveedor/proveedores.service';
@Component({
  selector: 'app-actualizarproveedores',
  templateUrl: './actualizarproveedores.component.html',
  styleUrls: ['./actualizarproveedores.component.css']
})
export class ActualizarproveedoresComponent implements OnInit {
  _id: string | null;
  public proveedor: any={};
  public proveedores : any[] = [];
  
constructor(private router:Router, private _provedor: ProveedoresService, private aRouter: ActivatedRoute) {
  this._id = this.aRouter.snapshot.paramMap.get('id');
}

ngOnInit(): void {
  this.EditProveedor();
}

addProveedor() {
  //Verificar existe el proveedor
  if (this._id !== null ) {
  //existe el producto se edita
    this._provedor.updateProveedor(this._id, this.proveedor).subscribe(data => {
    Swal.fire('Proveedor actualizado con exito!!')
    this.router.navigate(['mostrarproveedores']);
  }, error => {
    console.log(error);
    Swal.fire('error')
  })
  }}

  EditProveedor(){
    if (this._id !== null) {
      this._provedor.getProveedorId(this._id).subscribe((data:any)=>{
          this.proveedores=data.data;
          console.log(data);
      //Asignación de valores al formulario
          this.proveedor.nombre=this.proveedores[0].nombre;
          this.proveedor.telefono=this.proveedores[0].telefono;
          this.proveedor.direccion=this.proveedores[0].direccion,
          this.proveedor.email=this.proveedores[0].email;
          this.proveedor.producto=this.proveedores[0].producto;
    })}}
}