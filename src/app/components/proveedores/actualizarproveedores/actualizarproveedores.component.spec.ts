import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarproveedoresComponent } from './actualizarproveedores.component';

describe('ActualizarproveedoresComponent', () => {
  let component: ActualizarproveedoresComponent;
  let fixture: ComponentFixture<ActualizarproveedoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarproveedoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarproveedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
