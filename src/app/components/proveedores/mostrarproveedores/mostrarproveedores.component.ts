import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ProveedoresService } from 'src/app/services/proveedor/proveedores.service';
@Component({
  selector: 'app-mostrarproveedores',
  templateUrl: './mostrarproveedores.component.html',
  styleUrls: ['./mostrarproveedores.component.css']
})
export class MostrarproveedoresComponent implements OnInit {

  public proveedores : any[] = [];
  constructor( private _provedor : ProveedoresService) { }

  ngOnInit(): void {
    this.getTodosProveedores();
  }

  getTodosProveedores(){
    this._provedor.getProveedor()
    .subscribe((data: any) => {
        console.log(data);
        this.proveedores = data.data;
    })
  }
  deleteProveedor(id: any){
    this._provedor.deleteProveedor(id).subscribe(data => {
    Swal.fire('Proveedor eliminado con exito!!')
    this.getTodosProveedores();
    }, error => {
    console.log(error)
    })
  }

}
