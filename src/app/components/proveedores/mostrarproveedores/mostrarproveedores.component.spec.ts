import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarproveedoresComponent } from './mostrarproveedores.component';

describe('MostrarproveedoresComponent', () => {
  let component: MostrarproveedoresComponent;
  let fixture: ComponentFixture<MostrarproveedoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarproveedoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarproveedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
