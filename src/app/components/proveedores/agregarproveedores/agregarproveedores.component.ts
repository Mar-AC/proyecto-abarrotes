import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Proveedores } from 'src/app/config/config';
import { ProveedoresService } from 'src/app/services/proveedor/proveedores.service';

@Component({
  selector: 'app-agregarproveedores',
  templateUrl: './agregarproveedores.component.html',
  styleUrls: ['./agregarproveedores.component.css']
})
export class AgregarproveedoresComponent implements OnInit {
  public proveedor: any={};
  
  constructor(private _provedoreservice: ProveedoresService) {}
 
  ngOnInit(): void {
  }

  addProveedor(){
    this._provedoreservice.postProveedor(this.proveedor).subscribe(data => {
      Swal.fire('Proveedor registrado con exito!!')
      }, error => {
      console.log(error);
      Swal.fire('Error')
      })
      this.proveedor.nombre='';
      this.proveedor.telefono='';
      this.proveedor.direccion='';
      this.proveedor.email='';
      this.proveedor.producto='';
    }
}