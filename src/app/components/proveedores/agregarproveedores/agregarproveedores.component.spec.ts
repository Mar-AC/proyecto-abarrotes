import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarproveedoresComponent } from './agregarproveedores.component';

describe('AgregarproveedoresComponent', () => {
  let component: AgregarproveedoresComponent;
  let fixture: ComponentFixture<AgregarproveedoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarproveedoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarproveedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
