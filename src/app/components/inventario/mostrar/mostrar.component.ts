import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { InventariosService } from 'src/app/services/inventario/inventarios.service';

@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css']
})
export class MostrarComponent implements OnInit {

  public inventarios : any[] = [];
  constructor( private _inven: InventariosService) { }

  ngOnInit(): void {
    this.getTodosInventarios();
  }

  getTodosInventarios(){
    this._inven.getInventario()
    .subscribe((data: any) => {
      this.inventarios = data.data;
    })
  }

  deleteInventario(id: any){
    this._inven.deleteInventario(id).subscribe(data => {
    Swal.fire('Inventario eliminado con exito!!')
    this.getTodosInventarios();
    }, error => {
    console.log(error)
    }) }

}
