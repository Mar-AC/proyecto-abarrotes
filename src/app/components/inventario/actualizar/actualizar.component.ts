import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Inventarios } from 'src/app/config/config';
import { InventariosService } from 'src/app/services/inventario/inventarios.service';

@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.component.html',
  styleUrls: ['./actualizar.component.css']
})
export class ActualizarComponent implements OnInit {
  public inventarios: any[] = [];
  _id: string | null;
  public inventarioo: any={};

  constructor(private router:Router, private _inven: InventariosService, private aRouter: ActivatedRoute) {
  this._id = this.aRouter.snapshot.paramMap.get('id');
 }

  ngOnInit(): void {
    this.EditInventario();
  }

addInventario(){
  //Verificar existe el inventario
  if (this._id !== null ) {
  //existe el producto se edita
    this._inven.updateInventario(this._id, this.inventarioo).subscribe(data => {
    Swal.fire('Producto actualizado con exito!!')
    this.router.navigate(['mostrarinventario']);
    }, error => {
        console.log(error);
        Swal.fire('error')
  })}
}

  EditInventario(){
    if (this._id !== null) {
      this._inven.getInventarioId(this._id).subscribe((data:any)=>{
        this.inventarios=data.data;
        this.inventarioo.nombre= this.inventarios[0].nombre;
        this.inventarioo.cantidad= this.inventarios[0].cantidad;
        this.inventarioo.preciou= this.inventarios[0].preciou;
        this.inventarioo.preciov= this.inventarios[0].preciov;
        this.inventarioo.precioc= this.inventarios[0].precioc;
        this.inventarioo.fecha=this.inventarios[0].fecha;
        this.inventarioo.imagen=this.inventarios[0].imagen;
      })}}
}