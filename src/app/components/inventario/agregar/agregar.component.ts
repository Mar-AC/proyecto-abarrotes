import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Inventarios } from 'src/app/config/config';
import { InventariosService } from 'src/app/services/inventario/inventarios.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  public inventarioo: any={};
  formData = new FormData();
  selectedFile: File | null = null;

  constructor(private _inventarioservice: InventariosService) { }

  ngOnInit(): void {}

  addInventario(){
      console.log(this.inventarioo);
      this._inventarioservice.postInventario(this.inventarioo).subscribe(data => {
        Swal.fire('Exito','Producto registrado con exito!!','success');
        }, error => {
        console.log(error);
        Swal.fire('Error',"Ocurrio un error el ingresar datos, verifique sus datos",'error');
        })
  }

  onFileSelected(event: any): void {
        this.selectedFile = event.target.files[0] as File;
        this.onUpload();
  }

  onUpload(){
      if(this.selectedFile){
        this.formData.append('file',this.selectedFile);
        this._inventarioservice.postImg(this.formData).subscribe(
          (data: any) => {
            this.inventarioo.imagen=data.imageUrl;
          },
          (error: any) => {
            console.error(error);
          }
      )}else{
        Swal.fire('Advertencia', "Seleccione una imagen",'info');
      }
    }

}
