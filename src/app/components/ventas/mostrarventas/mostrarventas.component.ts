import { Component, OnInit } from '@angular/core';
import { VentasService } from 'src/app/services/venta/ventas.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-mostrarventas',
  templateUrl: './mostrarventas.component.html',
  styleUrls: ['./mostrarventas.component.css']
})
export class MostrarventasComponent implements OnInit {
  public products : any[]=[];
  public ventaas  : any[] = [];
  constructor( private _venta : VentasService) { }

  ngOnInit(): void {
    this.getTodasVentas();
  }

  getTodasVentas(){
    this._venta.getVentas()
    .subscribe((data: any) => {
        console.log(data);
        this.ventaas = data.data;
        console.log('ventaaas' , this.ventaas);
    })
  }

  deleteVenta(id:any){
    this._venta.deleteVenta(id).subscribe(data => {
      Swal.fire('Venta eliminado con exito!!')
      this.getTodasVentas();
      }, error => {
      console.log(error)
      })
    }

  }
