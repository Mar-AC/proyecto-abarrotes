import { Component, Inject, OnInit } from '@angular/core';
import { InventariosService } from 'src/app/services/inventario/inventarios.service';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import { VentasService } from 'src/app/services/venta/ventas.service';
import Swal from 'sweetalert2';
import { Ventas } from 'src/app/config/configvnt';
import { Inventarios } from 'src/app/config/config';

@Component({
  selector: 'app-agregarventas',
  templateUrl: './agregarventas.component.html',
  styleUrls: ['./agregarventas.component.css']
})

export class AgregarventasComponent implements OnInit {
  public f2=moment().format('MM-DD-YYYY');
  public inventarios : any[] = [];
  public products: any[] = [];
  public nproducts: any [] = [];
  public desc=0;
  cantidad:number=0;
  descuento:number=0;
  pago_cliente:number=0;
  cambio:number=0;
  subtotal:number=0;
  iva:number=1.16;
  total:number=0;

  constructor( private _inven: InventariosService, private _ventas:VentasService) { 
  }

  ngOnInit(): void {
    this.getTodosInventarios();
  }

  getTodosInventarios(){
    this._inven.getInventario()
    .subscribe((data: any) => {
      this.inventarios = data.data;
    })
  }
  
  addProducto(producto:Inventarios){
    if(this.cantidad>producto.cantidad)
      Swal.fire('CANTIDAD INSUFICIENTE!!')
    else{
      this.desc=(this.descuento/100)*producto.preciov;
      const nuevaVenta = {
        producto: producto.nombre,
        precioov: producto.preciov,
        cantidad: this.cantidad,
        descuento: this.desc*this.cantidad,
        subtot: (producto.preciov*this.cantidad)-(this.desc*this.cantidad),
      };
      this.nproducts.push(producto.nombre);
      this.products.push(nuevaVenta);
      const actualizar = producto;
      actualizar.cantidad=producto.cantidad-this.cantidad;
      this.disInv(actualizar._id,actualizar);

      this.products.forEach((product) => {
        product.subtot = (product.precioov * product.cantidad) - (this.desc * product.cantidad);
      });

      this.subtotal = this.products.reduce((total, productodos) => total + productodos.subtot, 0);
      this.total=this.subtotal * 1.16;
      this.iva=this.total-this.subtotal;
    }
  }

  addVenta(){
    const aux: [string] = this.nproducts as [string];
    const VENTA: Ventas = {
      productos:aux,
      cantidad:this.cantidad,
      subtotal: this.subtotal,
      iva:this.iva,
      total: this.total,
      fecha: this.f2,
      pago_cliente: this.pago_cliente,
      descuento:this.descuento,
      cambio: this.cambio,
    }
    this._ventas.postVentas(VENTA).subscribe((data: any) => {
      Swal.fire({
        title: 'Venta agregada con exito',
        text: "Imprimir Ticket?",
        showCancelButton: true,
        cancelButtonText:'NO',
        cancelButtonColor: '#d33',
      }).then((result) => {
        if (result.isConfirmed) {
          this.imprimirTicket();
          Swal.fire('LISTO!',)
          this.pago_cliente = 0;
            this.cambio = 0;
            this.subtotal = 0;
            this.iva = 1.16;
            this.total = 0;
        }
      });
            // Limpiar los campos después de agregar la venta
      

    }, error => {
        Swal.fire('Error')
    })
  }

  disInv(_id:any, put: any){
    this._inven.updateInventario(_id, put).subscribe(data=>{})
  }

  deleteProducto(elemento:number){
    this.products.splice(elemento,1);
  }

  Calcular(){
    this.cambio= this.pago_cliente - this.total;
  }

  imprimirTicket(){
    const  doc = new jsPDF('p','mm',[180,125.5]);
    const fecha = moment().format('MM-DD-YYYY');
    let yPos = 30;    
    //DATOS TICKET
    doc.line(125, 1, 125, 200)//Vericales // W comienza X altura Y termina Z seguimiento a X  
    doc.setFont('Bradley Hand ITC','bold')
    doc.setFontSize(18)
    doc.text("Viejos Panzones S.A. de C.V.", 25, 13);
    doc.setFontSize(11)
    let y = 38;

    //const ventaFormValues = this.ventaForm.value;
    doc.text("Instituto Tecnologico De cuautla",yPos,17)
    doc.text("Cuautla-Oaxaca S/N Col.Juan Morales", yPos,21);
    doc.text("627445 Cuautla Morelos, Mexico", yPos, 25)
    doc.text(`Fecha: ${fecha}`, yPos, 29);
    doc.line(5, 32, 125, 32)
    doc.line(5, 42, 125, 42)
    for (const producto of this.products) {
      doc.text("CANT", 5, 38);
      doc.text(`${producto.cantidad}`, 5, y + 10);
      doc.text("ARTICULO", 25,38)
      doc.text(`${producto.producto}`, 25, y + 10);
      doc.text("PREC.UNIT",65,38);
      doc.text(`$${producto.precioov}`, 65, y +10);
      doc.text("SUBTOTAL", 95, 38)
      doc.text(`$${producto.subtot}`, 95, y +10);
      
      y += 10; // Ajustar la posición vertical para la siguiente línea
    }
    doc.setFontSize(12);

    doc.text(`Subtotal: $${this.subtotal}`, 5, y + 30);
    doc.text(`Pago Cliente: $${this.pago_cliente}`, 5, y + 40);
    doc.text(`Cambio: $${this.cambio}`, 5, y + 50);
    doc.text(`Total: $${this.total}`, 5, y + 60);
    doc.text('---------------------------------------------------------------------------------', 5, y + 70);
    doc.text('Gracias por su compra', 43, y + 75);
    window.open(doc.output("bloburl"));
 }
}