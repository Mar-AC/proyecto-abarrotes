import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarventasComponent } from './agregarventas.component';

describe('AgregarventasComponent', () => {
  let component: AgregarventasComponent;
  let fixture: ComponentFixture<AgregarventasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarventasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarventasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
