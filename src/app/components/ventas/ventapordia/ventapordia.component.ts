import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { VentasService } from 'src/app/services/venta/ventas.service';
@Component({
  selector: 'app-ventapordia',
  templateUrl: './ventapordia.component.html',
  styleUrls: ['./ventapordia.component.css']
})
export class VentapordiaComponent implements OnInit {

  ventas: any []=[] ;
  ventasF: any;
  fecha: Date | null = null;

  constructor(private _VS:VentasService) { }

  ngOnInit(): void {
    this.getTodasVentas()
  }

  getTodasVentas(){
    this._VS.getVentas()
    .subscribe((data: any) => {
        this.ventas = data.data;
    })
  }

  Filtro(){
    if(this.fecha){
      this.ventasF = this.ventas.filter(venta=>moment(venta.fecha).isSame(this.fecha));
    }else{
      this.ventasF = [];
    }
  }

}
