import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentapordiaComponent } from './ventapordia.component';

describe('VentapordiaComponent', () => {
  let component: VentapordiaComponent;
  let fixture: ComponentFixture<VentapordiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentapordiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentapordiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
