import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentapormesComponent } from './ventapormes.component';

describe('VentapormesComponent', () => {
  let component: VentapormesComponent;
  let fixture: ComponentFixture<VentapormesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentapormesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentapormesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
