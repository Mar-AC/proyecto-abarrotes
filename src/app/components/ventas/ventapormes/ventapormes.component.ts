import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { VentasService } from 'src/app/services/venta/ventas.service';

@Component({
  selector: 'app-ventapormes',
  templateUrl: './ventapormes.component.html',
  styleUrls: ['./ventapormes.component.css']
})
export class VentapormesComponent implements OnInit {

  ventas: any [] = [];
  ventasF: any [] = [];
  mes: Date | null= null;

  constructor(private _VS: VentasService) { }

  ngOnInit(): void {
    this.getTodasVentas();
  }

  getTodasVentas(){
    this._VS.getVentas()
    .subscribe((data: any)=>{
      this.ventas = data.data;
    })
  }

  Filtro(){
    if (this.mes) {
      const selectedDate = moment(this.mes, 'YYYY-MM');
      this.ventasF = this.ventas.filter(venta => moment(venta.fecha).isSame(selectedDate, 'month'));
    } else {
      this.ventasF = [];
    }
  }

}
