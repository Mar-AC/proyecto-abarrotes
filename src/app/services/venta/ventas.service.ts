import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ventas } from 'src/app/config/configvnt';

@Injectable({
  providedIn: 'root'
})
export class VentasService {

  public url = 'http://localhost:3900';

  

 constructor( private _http : HttpClient ) { }

 getVentas():Observable<any> {
   const uri = `${this.url}/todos/ventas`;
   return this._http.get(uri);
 }

 getVentasMes(fechaMes:Date):Observable<any> {
  const uri = `${this.url}/venta/`;
  return this._http.get(uri + fechaMes);
 }

 getVentasDia(fechaDia:Date): Observable<any> {
   const uri = `${this.url}/venta/`;
   return this._http.get(uri + fechaDia);
 }

 postVentas(venta:Ventas): Observable<any> {
  const uri = `${this.url}/venta/nuevo`;
  return this._http.post(uri, venta);
}


 deleteVenta(_id:string): Observable<any> {
   const uri = `${this.url}/borrar/venta/`;
   return this._http.delete(uri + _id);
 }
   
 updateVenta(id:string, venta:Ventas): Observable<any> {
   const uri = `${this.url}/actualizar/venta/`;
   return this._http.put(uri + id, venta);
   }

}
