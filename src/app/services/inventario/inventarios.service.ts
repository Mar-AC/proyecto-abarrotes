import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inventarios } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class InventariosService {

  public url = 'http://localhost:3900';

  constructor( private _http : HttpClient ) { }

  getInventario():Observable<any> {
    const uri = `${this.url}/todo/inventario`;
    return this._http.get(uri);
  }
 
  getInventarioNombre(nombre:string):Observable<any> {
   const uri = `${this.url}/inventario/`;
   return this._http.get(uri + nombre);
  }

  getInventarioId(_id:string): Observable<any> {
    const uri = `${this.url}/id/inventario/`;
    return this._http.get(uri + _id);
  }
 
  postInventario(inventario:Inventarios): Observable<any> {
    const uri = `${this.url}/inv/nuevo`;
    return this._http.post(uri, inventario);
  }
 
  postImg(file: any): Observable<any> {
    const uri = `${this.url}/upload`;
    return this._http.post<any>(uri, file);
  }
 
  deleteInventario(_id:string): Observable<any> {
    const uri = `${this.url}/borrar/inventario/`;
    return this._http.delete(uri + _id);
  }
    
  updateInventario(_id:string, inventario:Inventarios): Observable<any> {
    const uri = `${this.url}/actualizar/inventario/`;
    return this._http.put(uri + _id, inventario);
    }

}
