import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Proveedores } from 'src/app/config/config';
@Injectable({
  providedIn: 'root'
})
export class ProveedoresService {


 public url = 'http://localhost:3900';


 constructor( private _http : HttpClient ) { }

 getProveedor():Observable<any> {
   const uri = `${this.url}/todos/proveedores`;
   return this._http.get(uri);
 }

 getProveedorNombre(nombre:string):Observable<any> {
  const uri = `${this.url}/proveedor/`;
  return this._http.get(uri + nombre);
 }

 getProveedorId(_id:string): Observable<any> {
   const uri = `${this.url}/id/proveedor/`;
   return this._http.get(uri + _id);
   }

 postProveedor(proveedor:Proveedores): Observable<any> {
   const uri = `${this.url}/proveedor/nuevo`;
   return this._http.post(uri, proveedor);
 }
 

 deleteProveedor(_id:string): Observable<any> {
   const uri = `${this.url}/borrar/proveedor/`;
   return this._http.delete(uri + _id);
 }
   
 updateProveedor(id:string, proveedor:Proveedores): Observable<any> {
   const uri = `${this.url}/actualizar/proveedor/`;
   return this._http.put(uri + id, proveedor);
   }
}
