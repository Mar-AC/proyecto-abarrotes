export class Proveedores{
    _id?:string;
    nombre: string;
    telefono: String;
    email: string;
    direccion: string;
    producto: string;
    constructor(nombre: string, telefono: string, email: string,direccion:string, producto: string){
      this.nombre = nombre;
      this.telefono = telefono;
      this.email= email;
      this.direccion = direccion;
      this.producto = producto;
    }
  }

  export class Inventarios{
    _id?:string;
    nombre: string;
    cantidad: number;
    preciou: number;
    preciov: number;
    precioc: number;
    fecha: string;
    imagen: string;
    constructor(nombre: string, cantidad: number, preciou: number, precioc:number, preciov: number,  fecha: string, imagen: string){
      this.nombre = nombre;
      this.cantidad = cantidad;
      this.preciou = preciou;
      this.preciov = preciov;
      this.precioc= precioc;
      this.fecha = fecha;
      this.imagen = imagen;
    }
  } 