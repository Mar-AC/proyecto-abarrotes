export class Ventas{
    _id?: String;
    productos: [string];
    cantidad: Number;
    subtotal: Number;
    iva: Number;
    total: Number;
    fecha: string;
    pago_cliente: Number;
    cambio: Number;
    descuento: Number;

    constructor(productos:[string],cantidad: Number, subtotal: Number, iva: Number, total: Number, fecha: string, pago_cliente: Number,descuento: Number, cambio: Number)
    {
      this.productos= productos;
      this.cantidad= cantidad;
      this.subtotal = subtotal;
      this.iva = iva;
      this.total = total;
      this.fecha = fecha;
      this.pago_cliente = pago_cliente;
      this.cambio=cambio;
      this.descuento= descuento;
    }
}