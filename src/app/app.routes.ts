import { Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ActualizarComponent } from "./components/inventario/actualizar/actualizar.component";
import { AgregarComponent } from "./components/inventario/agregar/agregar.component";
import { MostrarComponent } from "./components/inventario/mostrar/mostrar.component";
import { AgregarventasComponent } from "./components/ventas/agregarventas/agregarventas.component";
import { MostrarventasComponent } from "./components/ventas/mostrarventas/mostrarventas.component";
import { VentapordiaComponent } from "./components/ventas/ventapordia/ventapordia.component";
import { VentapormesComponent } from "./components/ventas/ventapormes/ventapormes.component";
import { MostrarproductosComponent } from "./components/productos/mostrarproductos/mostrarproductos.component";
import { MostrarproveedoresComponent } from "./components/proveedores/mostrarproveedores/mostrarproveedores.component";
import { AgregarproveedoresComponent } from "./components/proveedores/agregarproveedores/agregarproveedores.component";
import { ActualizarproveedoresComponent } from "./components/proveedores/actualizarproveedores/actualizarproveedores.component";

export const ROUTES: Routes=[
    {path:'home', component:HomeComponent},
    {path:'actualizarproductos/:id', component:ActualizarComponent},
    {path:'agregarproductos', component:AgregarComponent},
    {path:'mostrarinventario', component:MostrarComponent},
    {path:'mostrarproductos', component:MostrarproductosComponent},
    {path:'agregarventas', component:AgregarventasComponent},
    {path:'mostrarventas', component:MostrarventasComponent},
    {path:'ventaspordia', component:VentapordiaComponent},
    {path:'ventaspormes', component:VentapormesComponent},
    {path:'agregarproveedores', component:AgregarproveedoresComponent},
    {path:'mostrarproveedores', component:MostrarproveedoresComponent},
    {path:'actualizarproveedores/:id', component:ActualizarproveedoresComponent},
    {path:'',pathMatch:"full",redirectTo:'home'},
    {path:'**',pathMatch:"full",redirectTo:'home'}
]