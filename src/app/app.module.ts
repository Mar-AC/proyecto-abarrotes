import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { MostrarproductosComponent } from './components/productos/mostrarproductos/mostrarproductos.component';
import { AgregarproveedoresComponent } from './components/proveedores/agregarproveedores/agregarproveedores.component';
import { MostrarproveedoresComponent } from './components/proveedores/mostrarproveedores/mostrarproveedores.component';
import { ActualizarproveedoresComponent } from './components/proveedores/actualizarproveedores/actualizarproveedores.component';
import { AgregarventasComponent } from './components/ventas/agregarventas/agregarventas.component';
import { MostrarventasComponent } from './components/ventas/mostrarventas/mostrarventas.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MostrarComponent } from './components/inventario/mostrar/mostrar.component';
import { ActualizarComponent } from './components/inventario/actualizar/actualizar.component';
import { VentapordiaComponent } from './components/ventas/ventapordia/ventapordia.component';
import { VentapormesComponent } from './components/ventas/ventapormes/ventapormes.component';
import { AgregarComponent } from './components/inventario/agregar/agregar.component';
import { HttpClientModule } from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    MostrarproductosComponent,
    AgregarproveedoresComponent,
    MostrarproveedoresComponent,
    ActualizarproveedoresComponent,
    AgregarventasComponent,
    MostrarventasComponent,
    HomeComponent,
    NavbarComponent,
    MostrarComponent,
    ActualizarComponent,
    VentapordiaComponent,
    VentapormesComponent,
    AgregarComponent,    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
